package fr.afpa.Metier;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.Constantes.Constante;
import fr.afpa.bean.Meteorite;
import fr.afpa.bean.MeteoriteDeFeu;
import fr.afpa.bean.MeteoriteDeGlace;
import fr.afpa.bean.MeteoriteIceberg;
import fr.afpa.bean.MeteoriteSimple;
import fr.afpa.bean.MeteoriteZigzag;
import fr.afpa.bean.Vaisseau;
import fr.afpa.model.GestionVaisseau;
import junit.framework.TestCase;

public class TestVaisseau  extends TestCase {
		private List<Meteorite> listeMeteores;
		private Vaisseau vaisseau;
		private Meteorite meteorite;
		private Meteorite meteorite2;
		private GestionVaisseau gv;
	
	protected void setUp() throws Exception {
		super.setUp();	
		this.listeMeteores = new ArrayList<Meteorite>();
		this.meteorite = new MeteoriteIceberg();
		this.meteorite.setY(1000);		
		this.meteorite2 = new MeteoriteIceberg();
		this.meteorite2.setY(10);
		this.listeMeteores.add(this.meteorite);
		this.listeMeteores.add(this.meteorite2);
		this.vaisseau = new Vaisseau();
		this.gv = new GestionVaisseau();
	
	}
	/**
	 * Ibrahim Test JUnit 1 : Incrémentation Score
	 */
	public void testIncrementeScore() {
		int score = gv.incrementeScore(listeMeteores, vaisseau);
		assertEquals(5, score);
	}

	
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
