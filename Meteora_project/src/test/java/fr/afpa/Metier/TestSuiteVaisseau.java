package fr.afpa.Metier;

import junit.extensions.ActiveTestSuite;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestSuiteVaisseau extends TestCase {
	
	public TestSuiteVaisseau() {
		super();
	}
	
	public static TestSuite suite() {
		TestSuite suite = new ActiveTestSuite();
		suite.addTest(new TestSuite(TestVaisseau.class));
		suite.addTest(new TestSuite(TestLaser.class));
		
		return suite;
	}
	
	public static void main(String [] args) {
		junit.textui.TestRunner.run(suite());
	}

}
