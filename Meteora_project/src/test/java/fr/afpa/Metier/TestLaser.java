package fr.afpa.Metier;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.bean.Laser;
import fr.afpa.bean.Meteorite;
import fr.afpa.bean.MeteoriteIceberg;
import fr.afpa.bean.Vaisseau;
import fr.afpa.model.GestionLaser;
import fr.afpa.model.GestionVaisseau;
import junit.framework.TestCase;

public class TestLaser extends TestCase {
	
	private List<Laser> listeLaser;
	private Vaisseau vaisseau;
	private Laser laser;
	private Laser laser2;
	private GestionLaser gl;
	
	protected void setUp() throws Exception {
		super.setUp();	
		this.listeLaser = new ArrayList<Laser>();
		this.laser = new Laser();
		this.laser.mort();		
		this.laser2 = new Laser();
		this.gl = new GestionLaser();
		this.listeLaser.add(this.laser);
		this.listeLaser.add(this.laser2);
	
	}
	/**
	 * Ibrahim Test JUnit 2 : Nettoyage de la liste laser si laser mort
	 */
	public void testclean_liste_laser() {
		int nombrelaser = gl.clean_liste_laser(listeLaser);
		assertEquals(1, nombrelaser);
	}


	
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
}
