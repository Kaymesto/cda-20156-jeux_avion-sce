package fr.afpa.Interfaces;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import fr.afpa.view.GamePanel;
import fr.afpa.view.GameStart;


public class GameEventListener extends KeyAdapter implements ActionListener {
	
	private GamePanel board;

	
	public GameEventListener(GamePanel board) {
		this.board = board;

	}

public GameEventListener(GameStart gameStart) {
		// TODO Auto-generated constructor stub
	}

@Override
	public void keyReleased(KeyEvent e) {
		this.board.keyReleased(e);
	}

@Override
	public void keyPressed(KeyEvent e) {
		this.board.keyPressed(e);
	}

@Override
public void actionPerformed(ActionEvent e) {
	
	
}


}
