package fr.afpa.model;

import java.awt.Graphics;
import java.util.List;

import fr.afpa.bean.Laser;
import fr.afpa.bean.Meteorite;
import fr.afpa.bean.MeteoriteDeFeu;
import fr.afpa.bean.MeteoriteDeGlace;
import fr.afpa.bean.MeteoriteIceberg;
import fr.afpa.bean.MeteoriteSimple;
import fr.afpa.bean.MeteoriteZigzag;
import fr.afpa.bean.Vaisseau;
import fr.afpa.sounds.BanqueSon;
import fr.afpa.view.GamePanel;

public class GestionLaser {
	
	
	/** @Ibrahim
	 * Vérifie si il y a une collision avec des météorites et des lasers
	 * @param listeLaser
	 * @param listeMeteores
	 * @param vaisseau
	 */
	public void check_collision_laser(List <Laser> listeLaser, List<Meteorite> listeMeteores, Vaisseau vaisseau) {
		for (Laser l : listeLaser) {
			if (l.isMort() == false) {
				for(Meteorite m : listeMeteores) {
					if(m.isMort() == false) {
						if (l.getRect().intersects(m.getRect())) {
							if (vaisseau.getScore()<999) {
								if(m instanceof MeteoriteSimple){
									vaisseau.setScore(vaisseau.getScore()+2);
																	
								}else if(m instanceof MeteoriteDeFeu){
									vaisseau.setScore(vaisseau.getScore()+1);
									
								}else if(m instanceof MeteoriteDeGlace){
									vaisseau.setScore(vaisseau.getScore()+3);					
									
								}else if(m instanceof MeteoriteZigzag|| m instanceof MeteoriteIceberg){
									vaisseau.setScore(vaisseau.getScore()+5);
									
								}
							}
							if (vaisseau.getScore()>999)
							{vaisseau.setScore(999);}
							
							l.mort();
							m.mort();
							BanqueSon bs = new BanqueSon();
							bs.meteorite();
							break;
						}
					}
				}
			}
		}

		
	}
	
	/**
	 * Permet de dessiner les laser
	 * @param g
	 */
	public void dessiner_Laser(Graphics g, List <Laser> listeLaser, GamePanel gp) {
		for (Laser laser : listeLaser) {

		if (laser.isMort() == false) {
		g.drawImage(laser.getImage(),laser.getX(), laser.getY(), gp);		
		}}
	}
	
	/** @Ibrahim
	 * Vide la liste des lasers "morts"
	 * @param listeLaser
	 */
	public int clean_liste_laser(List <Laser> listeLaser) {

		for (int i = 0; i< listeLaser.size(); i++) {
			if (listeLaser.get(i).isMort() == true) {
				listeLaser.remove(i);
			}
			
		}
		return listeLaser.size();
	}
}
