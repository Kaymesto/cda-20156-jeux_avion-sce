package fr.afpa.model;

import java.awt.Graphics;
import java.util.List;

import javax.swing.ImageIcon;

import fr.afpa.Constantes.Constante;
import fr.afpa.bean.Meteorite;
import fr.afpa.bean.MeteoriteDeFeu;
import fr.afpa.bean.MeteoriteDeGlace;
import fr.afpa.bean.MeteoriteIceberg;
import fr.afpa.bean.MeteoriteSimple;
import fr.afpa.bean.MeteoriteZigzag;
import fr.afpa.bean.Vaisseau;
import fr.afpa.sounds.BanqueSon;
import fr.afpa.view.GamePanel;

public class GestionVaisseau {
	
	
	public int check_collision_vaisseau(List <Meteorite> listeMeteores, Vaisseau vaisseau, int compteur, BanqueSon bs, boolean en_jeu) {
		for(Meteorite m : listeMeteores) {
			if (m.isMort() == false) {

				if (m.getRect().intersects(vaisseau.getRect())) {
					if(m instanceof MeteoriteSimple){
						vaisseau.setVie_joueur((vaisseau.getVie_joueur()-1));
					}
					else if(m instanceof MeteoriteDeFeu){
						vaisseau.setVie_joueur((vaisseau.getVie_joueur()-2));
					}
					else if(m instanceof MeteoriteDeGlace){
						vaisseau.setVie_joueur((vaisseau.getVie_joueur()-2));
					}
					else if(m instanceof MeteoriteZigzag ){
						vaisseau.setVie_joueur((vaisseau.getVie_joueur()-2));
					}
					
					else if(m instanceof MeteoriteIceberg ){
						vaisseau.setVie_joueur((vaisseau.getVie_joueur()-4));
					}
					if(vaisseau.getVie_joueur()<0) {						
						vaisseau.setVie_joueur(0);}
					
					BanqueSon bsm = new BanqueSon();
					bsm.meteorite();
					m.mort();
					break;
				}	
			}
		}		
		if (vaisseau.getVie_joueur()<=0) {
		bs.musique_jeu(false);
		ImageIcon imageIcon = new ImageIcon(Constante.VAISSEAU_EXPLOD_IMAGE_URL);
		vaisseau.setImage(imageIcon.getImage());
		vaisseau.mort();
		if (compteur <1 && vaisseau.isMort() == true) {
			BanqueSon bst = new BanqueSon();
			bst.Vaisseau();
		}
		
		compteur++;
		}
		return compteur;
	}
	
	
	/**
	 * Incrémente le score du vaisseau lorsque une météorite est détruite par un laser ou est esquivée par le vaisseau
	 * @param listeMeteores
	 * @param vaisseau
	 */
	public int incrementeScore(List <Meteorite> listeMeteores, Vaisseau vaisseau) {
		
		for(Meteorite m : listeMeteores) {
			
			if (m.getY()>= Constante.HAUTEUR_FENETRE) {
				if (vaisseau.getScore()<999) {
					if(m instanceof MeteoriteSimple){
						vaisseau.setScore(vaisseau.getScore()+2);
						
					}else if(m instanceof MeteoriteDeFeu){
						vaisseau.setScore(vaisseau.getScore()+1);

						
					}else if(m instanceof MeteoriteDeGlace){
						vaisseau.setScore(vaisseau.getScore()+3);

						
					}else if(m instanceof MeteoriteZigzag || m instanceof MeteoriteIceberg){
						vaisseau.setScore(vaisseau.getScore()+5);
					}
				}
				if (vaisseau.getScore()>999)
				{vaisseau.setScore(999);}
			}}
		
		return vaisseau.getScore();
	}
	
	/**
	 * Permet de dessiner le vaisseau
	 * @param g
	 * @param vaisseau
	 * @param gp
	 */
	public void dessiner_Vaisseau(Graphics g, Vaisseau vaisseau, GamePanel gp) {
		g.drawImage(vaisseau.getImage(),vaisseau.getX(), vaisseau.getY(), gp);
	}

}
