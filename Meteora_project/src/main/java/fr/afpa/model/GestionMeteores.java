package fr.afpa.model;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.afpa.Constantes.Constante;
import fr.afpa.bean.*;
import fr.afpa.view.GamePanel;

public class GestionMeteores {
	
	/**
	 * Vide la liste des météorites "mortes"
	 * @param listeMeteores
	 */
	public void clean_liste_meteores(List<Meteorite> listeMeteores) {

		for (int i = 0; i< listeMeteores.size(); i++) {
			if (listeMeteores.get(i).isMort() == true) {
				listeMeteores.remove(i);
			}
			
		}
	}
	
	/** @Sofiane et Ibrahim
	 * Génération des météores
	 * @param listeMeteores
	 */
	public void initializeMeteores(List <Meteorite> listeMeteores) {
		int compteur = 0;
		boolean retry = false;
		
		for(Meteorite m : listeMeteores) {


			if (m.getY()>= Constante.HAUTEUR_FENETRE) {
				m.setMort(true);
				
				// listeMeteores.remove(m);
			}
			else { compteur++;}}
		
		if(compteur <Constante.NOMBRE_METEORITE) {
			retry = false;
			for (int i =0; i<Constante.NOMBRE_METEORITE-compteur; i++) {
			        int randMeteorite = randInt(1, 5);
			        Meteorite meteore;
					if (randMeteorite==1) {
						meteore = new MeteoriteZigzag();				
					}else if(randMeteorite==2) {
						meteore = new MeteoriteDeFeu();
					}else if(randMeteorite==3) {
						meteore= new MeteoriteIceberg();
					}else if(randMeteorite==4) {
						meteore= new MeteoriteDeGlace();
					}else  {
						meteore= new MeteoriteSimple();
					}
				
				meteore.setX(randInt(120,Constante.LONGUEUR_FENETRE-120));
				meteore.setDx(meteore.getX());

				for (int j = 0; j <1; j++) {
					retry = false;
				for(Meteorite m : listeMeteores) {
					if (m.isMort() == false) {
						if (meteore.getX() - m.getX()<120 && meteore.getX() - m.getX()>-120 || m.getX() - meteore.getX()<120 && m.getX() - meteore.getX()>-120) {
							retry = true;
							break;
						}	
					}
				}
				
				if (retry == true) {
					meteore.setX(randInt(120,Constante.LONGUEUR_FENETRE-120));
					meteore.setDx(meteore.getX());
					j=-1;
				}

				}
				
				listeMeteores.add(meteore);
			}
		}
		
	}
	
	
	/**
	 * Permet de dessiner les météorites
	 * @param g
	 */
	public void dessiner_Meteorite(Graphics g, List<Meteorite> listeMeteores, GamePanel gp) {
		for(Meteorite m : listeMeteores) {
			if (m.getY()>= Constante.HAUTEUR_FENETRE) {
				m.setMort(true);
			}
			if (m.isMort() == false) {
				g.drawImage(m.getImage(), m.getX(),m.getY(), gp);
			}
			
		}
	}
	
	
	/** @ibrahim
	 * Renvoi un int aléatoire compris entre le int min et max inclus
	 * @return
	 */
	private int randInt(int min, int max) {
		if (min >= max) {
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
}

}
