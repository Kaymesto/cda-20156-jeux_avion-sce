package fr.afpa.model;

	import java.io.EOFException;
import java.io.File;
	import java.io.FileInputStream;
	import java.io.FileNotFoundException;
	import java.io.FileOutputStream;
	import java.io.IOException;
	import java.io.ObjectInputStream;
	import java.io.ObjectOutputStream;
	import java.util.ArrayList;
	import java.util.Arrays;
	import java.util.Collections;

import fr.afpa.bean.Player;
import fr.afpa.control.ControlScor;


	public class GestionScore  {

			
		/**
		 * Methode pour s�railiser la liste mis en param � l'endroit du chemin �galement mis en param
		 * @param path
		 * @param l1
		 */
		public void insertList(ArrayList<Player> list) {
			Collections.sort(list);
			Collections.reverse(list);
			try {
				FileOutputStream fos;
				fos = new FileOutputStream(new File("C:\\ENV\\workspace\\GIT\\cda-20156-jeux_avion-sce\\Meteora_project\\temp\\spacer_score_cda20156.ser"));
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(list);
				oos.close();
				fos.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		/**
		 * Methoe pour deserialiser la liste 
		 * 
		 */

		@SuppressWarnings("unchecked")
		public ArrayList<Player> getList() {
			ArrayList<Player> list =new ArrayList<Player>();
			String chemin = "temp\\spacer_score_cda20156.ser";
			try {
				FileInputStream fis = new FileInputStream(new File(chemin));
				ObjectInputStream ios = new ObjectInputStream(fis);
				list = (ArrayList<Player>) ios.readObject();
				ios.close();
				
			} catch (IOException ioe) {
				ioe.printStackTrace();
				return null;
			}
			 catch (ClassNotFoundException c) {
				System.out.println("Class not found");
				c.printStackTrace();
				return null;
			}
			Collections.sort(list);
			Collections.reverse(list);
			return list;
		}
		
	}

