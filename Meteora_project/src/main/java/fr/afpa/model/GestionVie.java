package fr.afpa.model;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.ImageObserver;

import fr.afpa.bean.Vaisseau;
import fr.afpa.bean.Vie;
import fr.afpa.view.GamePanel;

public class GestionVie {
	
	/**
	 * Dessine le nombre de point de vie du joueur
	 * @param g
	 * @param g2
	 */
	public void dessiner_Vie(Graphics g, Graphics g2, Vaisseau vaisseau, Vie life, GamePanel gp) {
        String vie = ""+vaisseau.getVie_joueur();
        g.drawImage(life.getImage(),life.getX(), life.getY(), gp);
        g2.setFont(new Font("Roboto", Font.BOLD, 28));
        g2.setColor(Color.RED);
        g2.drawString(vie, 850, 50);

    }

}
