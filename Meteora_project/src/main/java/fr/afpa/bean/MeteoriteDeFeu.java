package fr.afpa.bean;

import java.awt.Rectangle;
import java.util.Random;

import javax.swing.ImageIcon;

import fr.afpa.Constantes.Constante;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class MeteoriteDeFeu extends Meteorite{

	
	public MeteoriteDeFeu() {
		initialize();

	}

	private void initialize() {
		ImageIcon imageIcon = BanqueImage.creationImage(Image.METEORITEDEFEU);
		setImage(imageIcon.getImage());
		rect.setBounds(x,y,Constante.LONGUEUR_MDF,Constante.LARGEUR_MDF);
		setY(0);
		
	}
	
	@Override
	public void move() {
		this.y += 2;
		rect.setBounds(x,y,Constante.LONGUEUR_MDF,Constante.LARGEUR_MDF);
		if (this.y>Constante.HAUTEUR_FENETRE) {
			this.mort();
		}
	}
	
}

