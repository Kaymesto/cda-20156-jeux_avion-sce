package fr.afpa.bean;

import java.awt.Rectangle;

import javax.swing.ImageIcon;

import fr.afpa.Constantes.Constante;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;

public class MeteoriteDeGlace extends Meteorite {

	
	public MeteoriteDeGlace() {
		intialize();

	}

	private void intialize() {
		ImageIcon imageIcon = BanqueImage.creationImage(Image.METEORITEDEGLACE);
		setImage(imageIcon.getImage());
		rect.setBounds(x,y,Constante.LONGUEUR_MDG,Constante.LARGEUR_MDG);
		setY(0);
		
	}
	
	@Override
	public void move() {
		this.y += 4;
		rect.setBounds(x,y,Constante.LONGUEUR_MDG,Constante.LARGEUR_MDG);
		if (this.y>Constante.HAUTEUR_FENETRE) {
			this.mort();
		}
	}}