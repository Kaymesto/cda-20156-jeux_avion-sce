package fr.afpa.bean;

import javax.swing.ImageIcon;

import fr.afpa.Constantes.Constante;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Vie extends Sprite {

	
	
	public Vie () {

		intialize();
	}

	private void intialize() {
		ImageIcon imageIcon = BanqueImage.creationImage(Image.LIFE);
		setImage(imageIcon.getImage());
		
		int start_x = Constante.POSITION_X_VIE;
		int start_y = Constante.POSITION_Y_VIE;
		
		setX(start_x);
		setY(start_y);
		
	}
	@Override
	public void move() {

	}

}
