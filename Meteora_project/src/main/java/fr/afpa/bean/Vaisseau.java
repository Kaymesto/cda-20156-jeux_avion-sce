package fr.afpa.bean;

import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.ImageIcon;

import fr.afpa.Constantes.Constante;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;
import lombok.Getter;

import lombok.Setter;

@Getter
@Setter

public class Vaisseau extends Sprite {
	private Rectangle rect= new Rectangle();
	private int vie_joueur;
	private int score;
	private String nom;

	public Vaisseau () {
		this.vie_joueur = 5;
		intialize();
	}

	private void intialize() {
		ImageIcon imageIcon = BanqueImage.creationImage(Image.VAISSEAU);
		setImage(imageIcon.getImage());
		
		int start_x = Constante.LONGUEUR_FENETRE / 2 - Constante.LARGEUR_VAISSEAU / 2;
		int start_y = Constante.HAUTEUR_FENETRE-100;
		
		setX(start_x);
		setY(start_y);
		rect.setBounds(start_x,start_y,50,55);
	}

	@Override
	public void move() {
		
		if (this.isMort() == false) {
		x += dx;
		y += dy;
		if (x<0) {
			x=0;
		}
		if (y<0) {
			y=0;
		}
		if (x>= Constante.LONGUEUR_FENETRE - Constante.LONGUEUR_VAISSEAU ) {
			x= Constante.LONGUEUR_FENETRE - Constante.LONGUEUR_VAISSEAU;
		}
		if (y>= Constante.HAUTEUR_FENETRE - Constante.LARGEUR_VAISSEAU - 26) {
			y= Constante.HAUTEUR_FENETRE - Constante.LARGEUR_VAISSEAU -26;
		}
        rect.setRect(x, y, Constante.LONGUEUR_VAISSEAU, Constante.LARGEUR_VAISSEAU);
		}
	}



	public void keyPressed(KeyEvent e) {
		int cle_touche = e.getKeyCode();
		if (this.isMort() == false) {
		if (cle_touche == KeyEvent.VK_LEFT || cle_touche == 81) {
			dx = -2;
			File f = new File(Constante.VAISSEAU_INCLINER_GAUCHE_IMAGE_URL);
			ImageIcon imageIcon = new ImageIcon( f.getAbsolutePath());			
			setImage(imageIcon.getImage());
		}
		
		if (cle_touche == KeyEvent.VK_RIGHT || cle_touche == 68) {
			dx = 2;
			File f = new File(Constante.VAISSEAU_INCLINER_DROITE_IMAGE_URL);
			ImageIcon imageIcon = new ImageIcon( f.getAbsolutePath());
			setImage(imageIcon.getImage());
		}
		
		if (cle_touche == KeyEvent.VK_UP || cle_touche == 90) {
			dy = -2;
		}
		
		if (cle_touche == KeyEvent.VK_DOWN || cle_touche == 83) {
			dy = 2;
		}
		}
	}
	
	
	public void keyReleased(KeyEvent e) {
		int cle_touche = e.getKeyCode();
		
		if (cle_touche == KeyEvent.VK_LEFT || cle_touche == 81) {
			dx = 0;
			File f = new File(Constante.VAISSEAU_IMAGE_URL);
			ImageIcon imageIcon = new ImageIcon( f.getAbsolutePath());
			setImage(imageIcon.getImage());		}
		
		if (cle_touche == KeyEvent.VK_RIGHT || cle_touche == 68) {
			dx = 0;
			File f = new File(Constante.VAISSEAU_IMAGE_URL);
			ImageIcon imageIcon = new ImageIcon( f.getAbsolutePath());
			setImage(imageIcon.getImage());
		}
		if (cle_touche == KeyEvent.VK_UP || cle_touche == 90) {
			dy = 0;
			File f = new File(Constante.VAISSEAU_IMAGE_URL);
			ImageIcon imageIcon = new ImageIcon( f.getAbsolutePath());			
			setImage(imageIcon.getImage());
		}
		if (cle_touche == KeyEvent.VK_DOWN || cle_touche == 83) {
			dy = 0;
			File f = new File(Constante.VAISSEAU_IMAGE_URL);
			ImageIcon imageIcon = new ImageIcon( f.getAbsolutePath());
			setImage(imageIcon.getImage());
		}
				
	}
}
