package fr.afpa.bean;

import java.awt.Rectangle;

import javax.swing.ImageIcon;

import fr.afpa.Constantes.Constante;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter

public class MeteoriteZigzag extends Meteorite {
	String nom;
	protected Rectangle rect= new Rectangle();
	boolean gauche;
	boolean droite;
	
	public MeteoriteZigzag() {
		initialize();
		gauche =true;
		droite = false;
	}
	
	private void initialize() {
		ImageIcon imageIcon = BanqueImage.creationImage(Image.METEORITEZIGZAG);
		setImage(imageIcon.getImage());
		rect.setBounds(x,y,Constante.LONGUEUR_MZZ,Constante.LARGEUR_MZZ);
		setY(0);
	}
	
	@Override
	public void move() {
		this.y += 2;
		
		if (gauche == true) {
			this.x-=2;
		}
		else if(droite == true) {
			this.x+=2;
		}
		if (this.dx - this.x >= 50) {
			gauche = false;
			droite = true;
		}
		else if (this.x - this.dx >= 50) {
			gauche = true;
			droite = false;			
		}
		rect.setBounds(x,y,Constante.LONGUEUR_MZZ,Constante.LARGEUR_MZZ);
		if (this.y>Constante.HAUTEUR_FENETRE) {
			this.mort();
		}
	}
	

}
