package fr.afpa.bean;

import javax.swing.ImageIcon;

import fr.afpa.Constantes.Constante;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;

public class MeteoriteIceberg extends Meteorite {

	
	public MeteoriteIceberg() {
		intialize();

	}

	private void intialize() {
		ImageIcon imageIcon = BanqueImage.creationImage(Image.METEORITEICEBERG);
		setImage(imageIcon.getImage());
		rect.setBounds(x,y,Constante.LONGUEUR_MIB,Constante.LARGEUR_MIB);
		setY(0);
		
	}
	
	@Override
	public void move() {
		this.y += 4;
		rect.setBounds(x,y,Constante.LONGUEUR_MIB,Constante.LARGEUR_MIB);
		if (this.y>Constante.HAUTEUR_FENETRE) {
			this.mort();
		}
	}}
