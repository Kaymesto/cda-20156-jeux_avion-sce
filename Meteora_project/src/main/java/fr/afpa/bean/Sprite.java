package fr.afpa.bean;

import java.awt.Image;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public abstract class Sprite {
	
	private Image image;
	private boolean mort;
	
	protected int x;
	protected int y;
	protected int dx;
	protected int dy;
	
	
	public abstract void move();

	public Sprite() {
		this.mort = false;
	}
	
	public void mort() {
		this.mort = true;
	}
}
