package fr.afpa.bean;

import java.awt.Rectangle;

import javax.swing.ImageIcon;

import fr.afpa.Constantes.Constante;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;

public class MeteoriteSimple extends Meteorite {
		
		public MeteoriteSimple () {
			initialize();
		}

		private void initialize() {
			ImageIcon imageIcon = BanqueImage.creationImage(Image.METEORITESIMPLE);
			setImage(imageIcon.getImage());
			rect.setBounds(x,y,Constante.LONGUEUR_METEORE,Constante.LARGEUR_METEORE);
			setY(0);
		}
		
		@Override
		public void move() {
			this.y += 4;
			rect.setBounds(x,y,Constante.LONGUEUR_METEORE,Constante.LARGEUR_METEORE);
			if (this.y>Constante.HAUTEUR_FENETRE) {
				this.mort();
			}
		}
		


}