package fr.afpa.bean;

import java.awt.Rectangle;
import java.util.Random;

import javax.swing.ImageIcon;

import fr.afpa.Constantes.Constante;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class Meteorite extends Sprite{
	protected Rectangle rect= new Rectangle();
	
	public Meteorite () {
		intialize();
	}

	private void intialize() {

	}
	
	@Override
	public void move() {

	}
	
}
