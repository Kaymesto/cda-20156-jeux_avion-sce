package fr.afpa.bean;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Player implements Serializable, Comparable<Player>{	

	private int score;
	private String nom;
	private String date;
	
	@Override
	public String toString() {
		return nom+";"+score+";"+date;
	}
	
	@Override
	public int compareTo(Player p) {
		
		String score1= ""+p.getScore();
		String score2=""+this.getScore();
		
		
		 if (this.getScore() < p.getScore()) {
			return -1;
		}
		else if (this.getScore() > p.getScore()) {
			return 1;
		}
		else {
		return 0;}
	}

}
