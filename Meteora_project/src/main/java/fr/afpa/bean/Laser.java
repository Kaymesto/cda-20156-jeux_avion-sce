package fr.afpa.bean;

import java.awt.Rectangle;

import javax.swing.ImageIcon;

import fr.afpa.Constantes.Constante;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class Laser extends Sprite {
	private Rectangle rect= new Rectangle();


	public Laser(int x, int y) {
		this.x = x;
		this.y = y;
		rect.setBounds(x, y, 50, 55);
		initialize();
	}
	
	public Laser() {
		initialize();
		rect.setBounds(x, y, 50, 55);
		
	}

	private void initialize() {
		rect.setBounds(x,y,50,55);
		ImageIcon imageIcon = BanqueImage.creationImage(Image.LASER);
		setImage(imageIcon.getImage());
		setX(x+Constante.LONGUEUR_VAISSEAU/2);
		setY(y);
		rect.setBounds(x, y, 13, 39);
	}

	@Override
	public void move() {
		rect.setBounds(x, y, 13, 39);
		this.y -= Constante.VITESSE_LASER;
		if (this.y<0) {
			this.mort();
		}
	}

}
