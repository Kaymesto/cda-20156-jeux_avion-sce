package fr.afpa.sounds;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class BanqueSon {
	
	private Clip clip;
	
	public BanqueSon(){
		
	}

	
	public void laser() {
		
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(".//Sounds//laser.wav"));
			clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
			
		} catch (Exception e) {
		e.printStackTrace();
		}
	}
	
	
	public void meteorite() {
		
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(".//Sounds//explosion.wav"));
			clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
			
		} catch (Exception e) {
		e.printStackTrace();
		}
	}
	
	public void Vaisseau() {
		
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(".//Sounds//gameOver.wav"));
			clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
			
		} catch (Exception e) {
		e.printStackTrace();
		}
	}
	
	public void musique_jeu(boolean play) {
		
		if (play == true) {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(".//Sounds//musicgame.wav"));
			clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
			clip.loop(Clip.LOOP_CONTINUOUSLY);
			
		} catch (Exception e) {
		e.printStackTrace();
		}
	}
	else {
		clip.stop();
	}
	}
	
	
}
