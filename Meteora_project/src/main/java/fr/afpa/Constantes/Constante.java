package fr.afpa.Constantes;

public interface Constante {

	
	// Titre de fenetre
	public static final String TITRE = "Meteora Game SCE";
	
	// Vitesse laser
	public static final int VITESSE_LASER = 5;
	
	// Taille de fenetre
	public static final int LONGUEUR_FENETRE = 900;
	public static final int HAUTEUR_FENETRE  = 750;
	
	// Definition du rafraichissement du jeu (1000 milliseconde / nombre de rafraichissement désiré)
	static final int NOMBRE_FPS = 60;
	public static final int RAFRAICHISSEMENT = (1000/NOMBRE_FPS);
	
	// Dimension vaisseau
	public static final int LONGUEUR_VAISSEAU = 52;
	public static final int LARGEUR_VAISSEAU = 52;
	
	// definition caractéristique météorite raccourci MDF
		public static final int NOMBRE_METEORITE = 4;
		public static final int Depart_Y = 0;
		public static final int LONGUEUR_METEORE = 51;
		public static final int LARGEUR_METEORE = 51;
	
	// definition caractéristique météorite de feu raccourci MDF
	public static final int LONGUEUR_MDF = 97;
	public static final int LARGEUR_MDF = 80;
	
	// definition caractéristique météorite de glace raccourci MDG
	public static final int LONGUEUR_MDG = 62;
	public static final int LARGEUR_MDG = 62;
	
	// definition caractéristique météorite ZigZag raccourci MZZ
	public static final int LONGUEUR_MZZ = 81;
	public static final int LARGEUR_MZZ = 81;
	
	// definition caractéristique météorite IceBerg raccourci MIB
	public static final int LONGUEUR_MIB = 120;
	public static final int LARGEUR_MIB = 120;
	
	
	// Chemin des images des objets
	public static final String METEORITESIMPLE_IMAGE_URL = "Images/MeteoriteSimple.png";
	public static final String METEORITEDEFEU_IMAGE_URL = "Images/meteoritedefeu4.png";
	public static final String METEORITEDEGLACE_IMAGE_URL = "Images//meteoritedeglace1.png";
	public static final String METEORITEZIGZAG_IMAGE_URL = "Images//MeteoriteZigZag.png";
	public static final String METEORITEICEBERG_IMAGE_URL = "Images//meteoriteIceberg2.png";
	public static final String LASER_IMAGE_URL = "Images//laser.png";
	public static final String VAISSEAU_IMAGE_URL = "Images//vaisseau.png";
	public static final String VAISSEAU_INCLINER_GAUCHE_IMAGE_URL = "Images//vaisseau.incliner.png";
	public static final String VAISSEAU_INCLINER_DROITE_IMAGE_URL = "Images//vaisseau.inclinerdroite.png";
	public static final String SCORE_BACKGROUND_URL = "Images//background2.jpg";
	public static final String PLANETE_BACKGROUND_URL = "Images//planete.jpg";
	public static final String METEORA_BACKGROUND_URL = "Images//background1.png";


	public static final String VAISSEAU_EXPLOD_IMAGE_URL = "Images//explod.gif";
	public static final String LIFE_IMAGE_URL = "Images//life.png";
	
	//Positionnement du score,nom,vie
	
	public static final int POSITION_X_VIE = 810;
	public static final int POSITION_Y_VIE = 25;
	public static final int POSITION_X_NOM = 400;
	public static final int POSITION_Y_NOM = 50;
	public static final int POSITION_X_SCORE = 30;
	public static final int POSITION_Y_SCORE = 50;


	
}
