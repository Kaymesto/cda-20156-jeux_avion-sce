package fr.afpa.control;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.ResourceBundle;

import fr.afpa.bean.Player;
import fr.afpa.model.GestionScore;

public class ControlScor {
	private ArrayList<Player>  list= new ArrayList<Player>();

	public ArrayList<Player> controlScor(Player p) {
		GestionScore gestScor = new GestionScore();
		// gestScor.insertList(list);	

		list= gestScor.getList();
		Collections.sort(list);
		if(list.size()<20||list==null) {
		list.add(p);
		}else if(p.getScore()>list.get(19).getScore()) {
			list.remove(19);
			list.add(p);
		}
		gestScor.insertList(list);	
		return list;
		
			}

}

