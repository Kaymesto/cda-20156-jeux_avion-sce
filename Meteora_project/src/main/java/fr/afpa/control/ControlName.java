package fr.afpa.control;

public class ControlName {
	
	public static boolean verifNom(String nom) {
		boolean verification = false;
		if(nom.length() >= 3 && nom.length() <= 6 && nom.contains(";") == false) {
			verification = true;
		}
		return verification;
	}

}
