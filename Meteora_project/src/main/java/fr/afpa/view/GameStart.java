package fr.afpa.view;

import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fr.afpa.Constantes.Constante;
import fr.afpa.Interfaces.GameEventListener;
import fr.afpa.control.ControlName;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;

public class GameStart extends JFrame implements ActionListener {
	
	private ImageIcon acceuilEcran;
	private JTextField champsPseudo;
	private JLabel label;
	private JButton button;

	
	public GameStart(){
		super();

		initializevariables();
		initializeLayout();

		
		
		
	}
		private void initializevariables() {
			setLayout(new GridBagLayout());
			
			
			this.acceuilEcran = BanqueImage.creationImage(Image.METEORA_BACKGROUND);

			this.champsPseudo = new JTextField(20);
			champsPseudo.setPreferredSize(new Dimension(100,30));
			
			this.label = new JLabel("Veuillez entrer votre nom : ");
			label.setFont(new Font("Roboto", Font.BOLD, 18));
			label.setForeground(Color.YELLOW);
			
			this.button = new JButton("Valider");
			button.setName("Valider");
			button.addActionListener(this);
			button.setMaximumSize(new Dimension(500,40));
			button.setBackground(Color.YELLOW);
					
			setContentPane(new JLabel(acceuilEcran));
			setLayout(new GridBagLayout());
			Box name = Box.createVerticalBox();
			name.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
			name.add(label);
			name.add(Box.createVerticalStrut(20));
			name.add(champsPseudo);
			name.add(Box.createVerticalStrut(20));
			name.add(button);
			add(name);
			setTitle(Constante.TITRE);
			pack();
			setSize(new Dimension(Constante.LONGUEUR_FENETRE, Constante.HAUTEUR_FENETRE));
			setDefaultCloseOperation(EXIT_ON_CLOSE);
			setLocationRelativeTo(null);
			setResizable(false);
			setVisible(true);
			boolean verif = false;
			
		}

		private void initializeLayout() {
			
			setFocusable(true);
			setPreferredSize(new Dimension(Constante.LONGUEUR_FENETRE, Constante.HAUTEUR_FENETRE));
		}
		
		  public void paintComponent(Graphics g) {
			  	super.paintComponents(g);

			    g.drawImage(acceuilEcran.getImage(), 0, 0, this);

			  }
		  
		  public void actionPerformed(ActionEvent e) {
			  JButton b = ((JButton)e.getSource());
			  String name = champsPseudo.getText();
			  if (button.getName().equals("Valider")) {
				  if (ControlName.verifNom(name) == true) {
					  	this.dispose();
					  	this.setVisible(false);
					  	EventQueue.invokeLater( () -> {
				    		new FrameDeJeu(name);
				    	});
				  }
			  			  
		  }

	}
}

