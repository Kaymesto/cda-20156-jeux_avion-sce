package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import fr.afpa.Constantes.Constante;
import fr.afpa.bean.Player;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;
import fr.afpa.model.GestionScore;

public class GameScore extends JFrame implements ActionListener{

	private ImageIcon ecranScore;
	private JLabel label;
	private JButton button;
	private JTable tableau;
	private String[] header= {"NOM", "Points", "Date"};  //entete du tableau
	private ArrayList <Player> list= new ArrayList<Player>(); 
	
	public GameScore(){
		super();
		initializevariables();
		initializeLayout();
		
		
		
	}
	
	private void initializevariables() {

		setLayout(new GridBagLayout());
		this.ecranScore = BanqueImage.creationImage(Image.SCORE_BACKGROUND);
		this.label = new JLabel("Classement :");
		label.setFont(new Font("Roboto", Font.BOLD, 44));
		label.setForeground(Color.YELLOW);
		DefaultTableModel model = new DefaultTableModel(header,0);
		GestionScore gestScor = new GestionScore();
		this.list = gestScor.getList();	

		 for (Object o : list) {
				String s=String.valueOf(((Player) o).getScore());
			 model.addRow(new String[]{((Player) o).getNom(),s, ((Player) o).getDate()});
			 }

		this.tableau = new JTable(model);
		
		 
		tableau.setPreferredScrollableViewportSize(new Dimension(450,63));
		tableau.setFillsViewportHeight(true);
		tableau.setEnabled(false);
		tableau.setBorder(BorderFactory.createLineBorder(Color.YELLOW,1));
		tableau.setGridColor(Color.YELLOW);
		this.button = new JButton("Retour");
		button.setName("Retour");
		button.addActionListener(this);
		JLabel jl = new JLabel(ecranScore);
		setContentPane(jl);
		JScrollPane js=new JScrollPane(tableau);
        js.setVisible(true);
       js.setFocusable(false);
        add(js);
		setLayout(new GridBagLayout());
		Box boxScore = Box.createVerticalBox();
		
		boxScore.setPreferredSize(new Dimension(400, 400));
		boxScore.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
		
		boxScore.add(label);
		boxScore.add(Box.createVerticalStrut(20));
		boxScore.add(tableau);
		add(boxScore);
		add(button);
		setTitle(Constante.TITRE);
		setIconImage(BanqueImage.creationImage(Image.VAISSEAU).getImage());
		pack();
		setSize(new Dimension(Constante.LONGUEUR_FENETRE, Constante.HAUTEUR_FENETRE));
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		boolean verif = false;
		
	}

	 protected void paintComponent(Graphics g) {
	        super.paintComponents(g);
	        g.drawImage(ecranScore.getImage(), 0, 0, this);
	    }

	private void initializeLayout() {
		
		setFocusable(true);
		setPreferredSize(new Dimension(Constante.LONGUEUR_FENETRE, Constante.HAUTEUR_FENETRE));
	}
	


	@Override
	public void actionPerformed(ActionEvent arg0) {
		JButton button = (JButton)arg0.getSource();
		if (button.getName().equals("Retour")){
			this.setVisible(false);
			this.dispose();
			GameAccueil ga = new GameAccueil();
		}
	}
}
