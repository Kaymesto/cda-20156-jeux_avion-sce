package fr.afpa.view;

import java.awt.Color;
import fr.afpa.model.*;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.util.Random;

import fr.afpa.Constantes.Constante;
import fr.afpa.Interfaces.GameEventListener;
import fr.afpa.Meteora_project.App;
import fr.afpa.bean.Laser;
import fr.afpa.bean.Meteorite;
import fr.afpa.bean.MeteoriteDeFeu;
import fr.afpa.bean.MeteoriteDeGlace;
import fr.afpa.bean.MeteoriteIceberg;
import fr.afpa.bean.MeteoriteSimple;
import fr.afpa.bean.MeteoriteZigzag;
import fr.afpa.bean.Player;
import fr.afpa.bean.Vaisseau;
import fr.afpa.bean.Vie;
import fr.afpa.control.ControlScor;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;
import fr.afpa.sounds.BanqueSon;
import lombok.Getter;
import lombok.Setter;
@Setter
public class GamePanel extends JPanel{
	
	private ImageIcon fondEcran;
	private GestionMeteores gm;
	private GestionLaser gl;
	private GestionVaisseau gv;
	private GestionVie gvie;
	private String nom;
	private Timer timer;
	private Vaisseau vaisseau = new Vaisseau();
	private Laser laser;
	private List <Laser> listeLaser;
	private boolean en_jeu = true;
	private List <Meteorite> listeMeteores;
	private Vie life;
	int compteur = 0;
	BanqueSon bs = new BanqueSon();
	DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date date = new Date();
	Player p = new Player();
	/**
	 * Constructeur
	 */
	public GamePanel(){
		
		initializevariables();
		initializeLayout();
		gm.initializeMeteores(this.listeMeteores);
		 bs.musique_jeu(true);
	}

	
	  	/**
	  	 * Procédure permettant d'initialiser les variables
	  	 */
	private void initializevariables() {
		this.life = new Vie();
		this.listeLaser = new ArrayList<Laser>();
		this.listeMeteores = new ArrayList<Meteorite>();
		this.vaisseau = new Vaisseau();
		this.vaisseau.setNom("");
		this.vaisseau.setNom(nom);
		this.laser = new Laser();
		this.fondEcran = BanqueImage.creationImage(Image.PLANETE_BACKGROUND);
		this.timer = new Timer(Constante.RAFRAICHISSEMENT, new GameLoop(this));
		this.timer.start();
		this.gm = new GestionMeteores();
		this.gl = new GestionLaser();
		this.gv = new GestionVaisseau();
		this.gvie = new GestionVie();
	}

	/**
	 * Ajout des keylistener, et parametre panel
	 */
	private void initializeLayout() {
		addKeyListener(new GameEventListener(this));
		setFocusable(true);
		setPreferredSize(new Dimension(Constante.LONGUEUR_FENETRE, Constante.HAUTEUR_FENETRE));
	}

	/**
	 * Permet de dessiner le background
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawImage(fondEcran.getImage(),0,0, null);
		
		dessiner_elements(g);
	}

	/**
	 * Dessine les différents éléments du jeu
	 * @param g
	 */
	private void dessiner_elements(Graphics g) {

		if (en_jeu == true) {
		gv.dessiner_Vaisseau(g, this.vaisseau, this);
		gvie.dessiner_Vie(g, g, this.vaisseau, this.life, this);
		gl.dessiner_Laser(g, this.listeLaser, this);
		gm.dessiner_Meteorite(g, this.listeMeteores, this);
		dessiner_Score(g);
		dessiner_Nom(g);
		}
		
		else if (timer.isRunning()) {
			timer.stop();	
			this.setVisible(false);	
			
			
			Frame[] fdj = FrameDeJeu.getFrames().clone();
			ArrayList <Frame> fdj2 = new ArrayList();
			
			for (int i = 0; i < fdj.length; i++) {
				if (fdj[i] != null) {
				fdj2.add(fdj[i]);}
			}
			for(Frame f : fdj2) {
							f.dispose();
			}
			fdj2.clear();
			p.setNom(nom);
			p.setDate(format.format(date));
			p.setScore(vaisseau.getScore());
			System.out.println(p);
			ControlScor controlScor= new ControlScor();
			controlScor.controlScor(p);
			GameAccueil ga = new GameAccueil();
		}
		
		Toolkit.getDefaultToolkit().sync();
	}

	
	/**
	 * Crée une boucle qui met à jour les données et les redessine
	 */
	public void doOneLoop() {
		update();
		repaint();
	}

	/** 
	 * Met à jour les coordonnées x et y des éléments de jeu
	 */
	private void update() {
		
		if (this.vaisseau.isMort() == false) {
		this.vaisseau.move();}
		else {
			File f = new File(Constante.VAISSEAU_EXPLOD_IMAGE_URL);
			ImageIcon imageIcon = new ImageIcon( f.getAbsolutePath());
			this.vaisseau.setImage(imageIcon.getImage());
		}
		for (Laser l : this.listeLaser) {
		l.move();}
		compteur = gv.check_collision_vaisseau(this.listeMeteores, this.vaisseau,this.compteur, this.bs, this.en_jeu);
		if (compteur > 200) {
			en_jeu = false;
		}
		gv.incrementeScore(this.listeMeteores, this.vaisseau);
		gl.check_collision_laser(this.listeLaser, this.listeMeteores, this.vaisseau);
		gl.clean_liste_laser(this.listeLaser);
		gm.clean_liste_meteores(this.listeMeteores);
		if (this.vaisseau.getVie_joueur()>0) {
		for(Meteorite m : this.listeMeteores) {
			m.move();
		}
		gm.initializeMeteores(this.listeMeteores);}
		
	}

	
	
	/**
	 * Affiche le score du joueur
	 * @param g
	 */
	private void dessiner_Score(Graphics g) {
		
		g.setFont(new Font("Roboto", Font.BOLD, 28));
        g.setColor(Color.YELLOW);
        String scor= Integer.toString(vaisseau.getScore());
        if (vaisseau.getScore()<10) {
        	scor="00"+scor;
        }else if(vaisseau.getScore()<100){
        	scor="0"+scor;
        }       
        g.drawString("Score :"+" "+ scor, 30, 50);
	}
	
	/**
	 * Affiche le nom du Joueur
	 * @param g
	 */
	private void dessiner_Nom(Graphics g) {
		
		g.setFont(new Font("Roboto", Font.BOLD, 28));
        g.setColor(Color.YELLOW);
        g.drawString(this.nom+""/*+" "+ vaisseau.getNom()*/, 400, 50);
	}	
	
	
	/** @ibrahim
	 * Permet d'appeler la méthode keyreleased de vaisseau lorsque la touche est relachée
	 * @return
	 */
	public void keyReleased(KeyEvent e) {
		this.vaisseau.keyReleased(e);
	}
	
	
	
	/** @ibrahim
	 * Permet d'effectuer un tir laser si la touche espace est pressée
	 * @return
	 */
	public void keyPressed(KeyEvent e) {
		this.vaisseau.keyPressed(e);
		
		int code_cle = e.getKeyCode();
		
		if (code_cle == e.VK_SPACE) {
			int laserX = vaisseau.getX();
			int laserY = vaisseau.getY();
			int compteur = 0;
			if (en_jeu == true) {
				for (Laser l : this.listeLaser) {
					if (l.isMort()== false) {
						compteur++;
					}
				}
				if (compteur <10 && vaisseau.isMort() == false) {
				this.listeLaser.add(new Laser(laserX, laserY));
				BanqueSon bs = new BanqueSon();
				bs.laser();}
			}
		}
		
	}
	
	/** @ibrahim
	 * Renvoi un int aléatoire compris entre le int min et max inclus
	 * @return
	 */
	private int randInt(int min, int max) {
		if (min >= max) {
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
}


/** @ibrahim
 * Renvoi si le jeu est en cours ou la partie est finie
 * @return
 */
	public boolean isEn_jeu() {
		return en_jeu;
	}



}
