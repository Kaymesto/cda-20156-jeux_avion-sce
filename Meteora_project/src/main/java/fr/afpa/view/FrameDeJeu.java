package fr.afpa.view;

import javax.swing.JFrame;

import fr.afpa.Constantes.Constante;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;

public class FrameDeJeu extends JFrame {

	public FrameDeJeu(String name) {
		initializeLayout(name);
	}

	private void initializeLayout(String name) {

		GamePanel gamePanel = new GamePanel();
		gamePanel.setNom(name);
		add(gamePanel);
		
		add(gamePanel);
	
		setTitle(Constante.TITRE);
		setIconImage(BanqueImage.creationImage(Image.VAISSEAU).getImage());
		pack();
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		boolean verif = false;
		
	}
	
	public void fermetureFrame() {
		setVisible(false);
		dispose();
	}
	
	
	
	

	
	
}
