package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import fr.afpa.Constantes.Constante;
import fr.afpa.images.BanqueImage;
import fr.afpa.images.Image;

public class GameAccueil extends JFrame implements ActionListener {
	private ImageIcon acceuilEcran;
	private JButton lancerPartie;
	private JButton afficherScore;
	private JButton quitterJeu;
	
	public GameAccueil(){
		super();
		initializevariables();
		initializeLayout();
		
		
		
	}
		private void initializevariables() {

			setLayout(new GridBagLayout());
			this.acceuilEcran = BanqueImage.creationImage(Image.METEORA_BACKGROUND);
			
			this.lancerPartie = new JButton("Lancer une Partie");
			lancerPartie.setName("Lancer une Partie");
			lancerPartie.setMaximumSize(new Dimension(500, 50));
			lancerPartie.setBackground(Color.YELLOW);
			
			this.afficherScore = new JButton("Afficher les scores");
			afficherScore.setName("Afficher les scores");
			afficherScore.setMaximumSize(new Dimension(500, 50));
			afficherScore.setBackground(Color.YELLOW);
			
			this.quitterJeu = new JButton("Quitter le jeu");
			quitterJeu.setName("Quitter le jeu");
			quitterJeu.setMaximumSize(new Dimension(500, 50));
			quitterJeu.setBackground(Color.YELLOW);
			
			
			lancerPartie.addActionListener(this);
			afficherScore.addActionListener(this);
			quitterJeu.addActionListener(this);
			setContentPane(new JLabel(acceuilEcran));
			setLayout(new GridBagLayout());
			Box boxAccueil = Box.createVerticalBox();
			boxAccueil.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
			boxAccueil.add(Box.createVerticalStrut(100));
			boxAccueil.add(lancerPartie);
			boxAccueil.add(Box.createVerticalStrut(40));
			boxAccueil.add(afficherScore);
			boxAccueil.add(Box.createVerticalStrut(40));
			boxAccueil.add(quitterJeu);
			boxAccueil.add(Box.createVerticalStrut(40));
			add(boxAccueil);
			setTitle(Constante.TITRE);
			setIconImage(BanqueImage.creationImage(Image.VAISSEAU).getImage());
			pack();
			setSize(new Dimension(Constante.LONGUEUR_FENETRE, Constante.HAUTEUR_FENETRE));
			setLocationRelativeTo(null);
			setResizable(false);
			setVisible(true);
			boolean verif = false;
			
		}

		 protected void paintComponent(Graphics g) {
		        super.paintComponents(g);
		        g.drawImage(acceuilEcran.getImage(), 0, 0, this);
		    }

		private void initializeLayout() {
			
			setFocusable(true);
			setPreferredSize(new Dimension(Constante.LONGUEUR_FENETRE, Constante.HAUTEUR_FENETRE));
		}
		
	
		  
		  public void actionPerformed(ActionEvent e) {
			  JButton b = ((JButton)e.getSource());
			  if (b.getName().equals("Lancer une Partie")) {
				  GameStart gameStart = new GameStart();
				  this.dispose();
		}	
			  if (b.getName().equals("Afficher les scores")) {
				  GameScore gameScore = new GameScore();
				  this.dispose();
				  this.setVisible(false);
		}	
			  if (b.getName().equals("Quitter le jeu")) {
			System.exit(0);
		}	
		  }

	}

