package fr.afpa.images;

import javax.swing.ImageIcon;

import fr.afpa.Constantes.Constante;

public class BanqueImage {
	
	public static ImageIcon creationImage(Image image) {
		
		ImageIcon imageIcon = null;
		
		switch (image) {
		case METEORITESIMPLE:
				imageIcon = new ImageIcon(Constante.METEORITESIMPLE_IMAGE_URL);
			break;
		case METEORITEDEFEU:
			imageIcon = new ImageIcon(Constante.METEORITEDEFEU_IMAGE_URL);
		break;
		case METEORITEDEGLACE:
			imageIcon = new ImageIcon(Constante.METEORITEDEGLACE_IMAGE_URL);
		break;
		case METEORITEZIGZAG:
			imageIcon = new ImageIcon(Constante.METEORITEZIGZAG_IMAGE_URL);
		break;		
		case METEORITEICEBERG:
			imageIcon = new ImageIcon(Constante.METEORITEICEBERG_IMAGE_URL);
		break;

		case LASER:
			imageIcon = new ImageIcon(Constante.LASER_IMAGE_URL);
		break;
		case VAISSEAU:
			imageIcon = new ImageIcon(Constante.VAISSEAU_IMAGE_URL);
		break;
		case SCORE_BACKGROUND:
			imageIcon = new ImageIcon(Constante.SCORE_BACKGROUND_URL);
		break;
		case METEORA_BACKGROUND:
			imageIcon = new ImageIcon(Constante.METEORA_BACKGROUND_URL);
		break;
		case PLANETE_BACKGROUND:
			imageIcon = new ImageIcon(Constante.PLANETE_BACKGROUND_URL);
		break;
		case LIFE:
			imageIcon = new ImageIcon(Constante.LIFE_IMAGE_URL);
		break;
		default:
			break;
		}
		
		return imageIcon;
	}

}
